import React from 'react';
import './App.css';

import MessagesComponent from "./Components/MessagesComponent/MessagesComponent";

function App() {
    return (
        <div className="App">
            <MessagesComponent></MessagesComponent>
        </div>
    );
}

export default App;
