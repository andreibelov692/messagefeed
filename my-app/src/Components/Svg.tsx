import React from 'react';

interface SvgProps {
    name: string;
    width?: string | number;
    height?: string | number;
}

const Svg = ({name, width = 16, height = 16}: SvgProps) => (
    <img src={require(`../images/${name}.svg`)} alt={`${name} icon`} width={width} height={height}/>
);

export default Svg;
