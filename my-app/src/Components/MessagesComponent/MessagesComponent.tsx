import React, {useEffect} from 'react';
import axios from 'axios';
import s from './messageComponent.module.css';
import MessagesList from '../MessagesList/MessagesList';

interface IAttachment {
    type: string;
    url: string;
}

interface IMessage {
    id: string;
    content: string;
    author?: string[];
    date: string;
    url: string;
    attachments?: IAttachment[];
}

function MessagesComponent() {
    const [messages, setMessages] = React.useState<IMessage[]>([]);
    const [sortedMessages, setSortedMessages] = React.useState<IMessage[]>([]);
    const [sortByDate, setSortByDate] = React.useState<boolean>(true);
    const [selectedButton, setSelectedButton] = React.useState<string>('new');

    useEffect(() => {
        const fetchData = async () => {
            const data = new FormData();
            data.append('actionName', 'MessagesLoad');

            try {
                const response = await axios.post('http://a0830433.xsph.ru/', data);
                setMessages(response.data.Messages);
                setSortedMessages(response.data.Messages);
            } catch (error) {
                console.log(error);
            }
        };

        fetchData();

        const intervalId = setInterval(fetchData, 5000);

        return () => clearInterval(intervalId);
    }, []);

    const sortMessages = (isAscending: boolean) => {
        const sorted = [...sortedMessages].sort((a, b) => {
            if (isAscending) {
                return new Date(a.date).getTime() - new Date(b.date).getTime();
            } else {
                return new Date(b.date).getTime() - new Date(a.date).getTime();
            }
        });

        setSortByDate(isAscending);
        setSortedMessages(sorted);

        // Обновляем выбранную кнопку
        if (isAscending) {
            setSelectedButton('new');
        } else {
            setSelectedButton('old');
        }
    };

    return (
        <div className={s.list}>
            <div>
                <button
                    className={`${s.button} ${selectedButton === 'new' ? s.selected : ''}`}
                    onClick={() => sortMessages(true)}
                >
                    Новые
                </button>
                <button
                    className={`${s.button} ${selectedButton === 'old' ? s.selected : ''}`}
                    onClick={() => sortMessages(false)}
                >
                    Старые
                </button>
            </div>
            <div className={s.slideUp}>
                <MessagesList messages={sortedMessages} selectedButton={selectedButton}/>
            </div>
        </div>
    );
}

export default MessagesComponent;