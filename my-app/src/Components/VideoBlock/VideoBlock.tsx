import React from 'react';
import s from './VideoBlock.module.css'

interface AttachmentType {
    url: string;
    type: string;
}

interface VideoBlockProps {
    attachment: AttachmentType;
}

const VideoBlock: React.FC<VideoBlockProps> = ({attachment}) => (
    <video className={s.video} controls>
        <source src={attachment.url} type={attachment.type}/>
        Ваш браузер не поддерживает элемент video.
    </video>
);

export default VideoBlock;