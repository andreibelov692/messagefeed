import React, {useEffect, useState} from 'react';
import s from './messages.module.css';
import Svg from '../Svg';
import VideoBlock from '../VideoBlock/VideoBlock';

interface IAttachment {
    type: string;
    url: string;
}

interface IMessage {
    id: string;
    content: string;
    author?: string[];
    date: string;
    attachments?: IAttachment[];
}

interface IMessagesListProps {
    messages: IMessage[];
    selectedButton: string;
}

function MessagesList(props: IMessagesListProps) {
    const {messages} = props;


    const getTimeFromDate = (dateStr: string) => {
        const date = new Date(dateStr);
        const hours = date.getHours();
        const minutes = date.getMinutes();
        return `${hours.toString().padStart(2, '0')}:${minutes.toString().padStart(2, '0')}`;
    };

    const [favoritedMessages, setFavoritedMessages] = useState<string[]>([]);

    useEffect(() => {
        const storedMessages = localStorage.getItem('favoritedMessages');
        console.log(favoritedMessages)
        if (storedMessages) {
            setFavoritedMessages(JSON.parse(storedMessages));
        }
    }, []);

    const handleFavoriteClick = (id: string) => {
        if (favoritedMessages.includes(id)) {
            setFavoritedMessages(favoritedMessages.filter((msgId) => msgId !== id));
            localStorage.setItem('favoritedMessages', JSON.stringify(favoritedMessages.filter((msgId) => msgId !== id)));
        } else {
            const newFavoritedMessages = [...favoritedMessages, id];
            setFavoritedMessages(newFavoritedMessages);
            localStorage.setItem('favoritedMessages', JSON.stringify(newFavoritedMessages));
        }
    };

    const isMessageFavorited = (id: string) => {
        return favoritedMessages.includes(id);
    };

    return (
        <>
            {messages.map((message) => {
                const isFavorited = isMessageFavorited(message.id);

                return (
                    <div className={s.wrapper} key={message.id}>
                        <div className={s.fadeIn}>
                            <div className={s.messageWrapper}>
                                <div className={s.user}>
                                    <Svg width={36} height={36} name={'svg'}/>
                                    {getTimeFromDate(message.date)}
                                </div>

                                <div className={s.messageInfo}>
                                    <div>
                                        <div className={s.author}>
                                            {message.author}
                                            <span className={s.publication}>Текст поста в соцю сетях, если это комментарий</span>
                                            <div className={s.buttonGroup}>
                                                <div className={s.button}>
                                                    Левый
                                                </div>
                                                <div className={s.button}>
                                                    Центр
                                                </div>
                                                <div className={s.button}>
                                                    Правый
                                                </div>
                                                <div className={s.icons}>
                                                    <Svg name={'iconArrowRight'} width={18} height={20}></Svg>
                                                    <Svg name={'iconBox'} width={18} height={18}></Svg>
                                                    <Svg name={'iconGear'} width={20} height={20}></Svg>
                                                </div>
                                                <div onClick={() => handleFavoriteClick(message.id)}>
                                                    <Svg
                                                        name={isFavorited ? 'favorite' : 'favorite2'}
                                                        width={20}
                                                        height={23}
                                                    />
                                                </div>
                                            </div>
                                        </div>

                                        <div className={s.text}>{message.content}</div>
                                        <a href="#" className={s.openNews}>
                                            Далее
                                        </a>
                                    </div>
                                    <div>
                                        {message.attachments &&
                                            message.attachments.map((attachment) => {
                                                if (attachment.type.startsWith('video')) {
                                                    return (
                                                        <div className={s.url} key={attachment.url}
                                                             data-testid={`video-${attachment.url}`}>
                                                            <a href={attachment.url} target="_blank">
                                                                <VideoBlock attachment={attachment}/>
                                                            </a>
                                                        </div>
                                                    );
                                                } else {
                                                    return (
                                                        <div
                                                            className={s.url}
                                                            key={attachment.url}
                                                            style={{backgroundImage: `url(${attachment.url})`}}
                                                        ></div>
                                                    );
                                                }
                                            })}
                                    </div>
                                </div>

                            </div>
                            <div className={s.hashtags}>
                            <span className={`${props.selectedButton === 'old' ? s.old : s.new}`}>
                                #Новое
                            </span>
                                <span className={s.expert}>
                                #Эксперт
                            </span>
                            </div>
                        </div>

                    </div>
                );
            })}
        </>
    );
}

export default MessagesList;